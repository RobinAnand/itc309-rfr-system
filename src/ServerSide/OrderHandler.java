/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

/**
 *
 * @author 11628068
 */
public class OrderHandler {
   
  static String tbNumber;
  Order_DB orderDB= new Order_DB();
   int updateTtlItems=0;
   double updateTtlPrice=0;
    
  public void addToItems(int odNo, String itmNam, String itmQty, String totItmPrc, String tbNo, String itmNotes){
      
      
   orderDB.addToOrderItems(odNo, itmNam, itmQty, totItmPrc, itmNotes, tbNo);
   
  }
  
  
  public void getOrderNumber(){
      
     
      orderDB.viewOrderDetails();
      
    
  }
  
  public void updateOurTables(String tblNo,  int ordNo, int totItmQty, double totOrPrice){
      
      orderDB.addToOurTables(tblNo, String.valueOf(ordNo), String.valueOf(totItmQty), String.valueOf(totOrPrice));
      
      
  }
  
  
  
  
  public void addToOrder(String tableNum, String customerName, int numberOfItems, double ttlOrdPri ){
      
      
      
     orderDB.viewOrderDetails();
     
   if(ServerHandler.convertOrderNumber >0){
       
       
       
        orderDB.readOrders(ServerHandler.convertOrderNumber);
       
        updateTtlItems=Order_DB.getTotalItems+numberOfItems;
        
        updateTtlPrice=Order_DB.getTotalPrice+ttlOrdPri;
        
       
       orderDB.updateOrders(ServerHandler.convertOrderNumber, updateTtlItems, updateTtlPrice);
      
       updateOurTables(tableNum, ServerHandler.convertOrderNumber, updateTtlItems, updateTtlPrice);
       
       updateTtlItems=0;
       updateTtlPrice=0;
       
   }
   
   
   else if(ServerHandler.convertOrderNumber==0){
       
          
       orderDB.addToOrder(customerName, numberOfItems, tableNum, ttlOrdPri);
       
       
       
       getOrderNumber();
       int newOrderNumber=orderDB.orderNumber;
       //String con=String.valueOf(newOrderNumber);
       
       
       updateOurTables(tableNum, newOrderNumber, numberOfItems, ttlOrdPri);
      
      
      
      
      
  }            
}
  
}
