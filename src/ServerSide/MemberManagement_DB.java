/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

import static ServerSide.Menu_DB.feedback;
import static ServerSide.Menu_DB.itemAlreadyExist;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author 11628068
 */
public class MemberManagement_DB {
    
    static String userAlreadyExist;
    static String emailAlreadyExist;
    static String executionFeedback;
    static String getUserName;
    static String getUserPassword;
    static String getUserEmail;
    static String getFirstName;
    static String getLastName;
    static String getPoints;
    static String sendMemberDetails="";
    static String recordFeedback;
    
public void checkMembers(String userName) {
    
    
      try {
          
             String query = "SELECT * FROM memberaccounts WHERE userName = "
              + "'" + userName + "'";
   
           // Execute query
             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
             searchFromMembers(rs);
             
            }
            catch(SQLException ex) {
              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
              alert2.showAndWait(); 
           }
           
        }
     
     
     public void searchFromMembers(ResultSet rs) throws SQLException {
      
      if (rs.next()){
      
           userAlreadyExist=rs.getString(1);
         
           
           System.out.println(userAlreadyExist);
          
      }
      
     }
     
     
     public void checkEmails(String email) {
    
    
      try {
          
             String query = "SELECT * FROM memberaccounts WHERE email = "
              + "'" + email + "'";
   
           // Execute query
             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
             searchFromEmails(rs);
             
            }
            catch(SQLException ex) {
              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
              alert2.showAndWait(); 
           }
           
        }
     
     
     public void searchFromEmails(ResultSet rs) throws SQLException {
      
      if (rs.next()){
      
           emailAlreadyExist=rs.getString(3);
           
           System.out.println(emailAlreadyExist);
          
      }
      
     }
     
     
     
     public void viewMemberDetails(String userName) {
    
    
      try {
          
             String query = "SELECT * FROM memberaccounts WHERE userName = "
              + "'" + userName + "'";
   
           // Execute query
             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
             searchMemberDetails(rs);
             
            }
            catch(SQLException ex) {
              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
              alert2.showAndWait(); 
           }
           
        }
     
     
     public void searchMemberDetails(ResultSet rs) throws SQLException {
      
      while (rs.next()){
      
           getUserName=rs.getString(1);
           getUserPassword=rs.getString(2);
           getUserEmail=rs.getString(3);
           getFirstName=rs.getString(4);
           getLastName=rs.getString(5);
           getPoints=rs.getString(6);
           
           sendMemberDetails+=getUserName+"//"+getUserPassword+"//"+getUserEmail+"//"+getFirstName+"//"+getLastName+"//"+getPoints;
           
           
           System.out.println("FetchingDetails");
          
      }
      
     }
     
     
    public void updateMemberDetails(String uName, String pass, String email, String fName, String lName){
        
        String query = "UPDATE memberAccounts " + 
          "SET password = '" + pass+ "'," +
                "firstName='"+ fName+ "'," +
               "lastName='"+ lName+ "'"+
                "WHERE userName = '" + uName + "';";
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(query);
                recordFeedback="Record Updated";
        }
        catch(SQLException ex) {
            System.out.println(ex);
        }
        
        
     
     
     
    }
      
    
    
    
    public  void  insertMember(String userName, String password, String email, String firstName, String lastName){ 
        
       // System.out.println("Databse"+ tableNumber);
        //System.out.println("Databse"+ status);
        int points=0;
        
        String insertStmt =
                "INSERT INTO memberaccounts(userName, password, email, firstName, lastName, points)VALUES('" +
                userName + "','" +
                password + "','" +
                email + "','" +
                firstName + "','" +
                lastName + "','" +
                points + "');";
        
//        String query = "UPDATE memberAccounts " + 
//          "SET userName = '" + userName+ "'," +
//               "password='"+ password+ "'," +
//               "email='"+ email+ "'," +
//               "firstName='"+ email+ "'," +
//               "lastName='"+ lastName+ "'"+
//                "WHERE userName = '" + userName + "';";
        
            try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(insertStmt);
                executionFeedback="Record Updated";
            }
            catch(SQLException ex) {
            System.out.println(ex);
            }
            }


}
