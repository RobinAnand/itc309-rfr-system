/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

/**
 *
 * @author 11628068
 */
public class FoodItemHandler {
    
    String itemCode;
    String itemName;
    int numberOfItems;
    int quantity;
    boolean isSelected;
    String itemPrice;
    
    
    
    
public FoodItemHandler(){
   
    
}


public FoodItemHandler (String itemCode, String itemName, String itemPrice){
    
     this.itemCode=itemCode;
    this.itemName=itemName;
    this.itemPrice=itemPrice;
    
    
}
public FoodItemHandler (String itemName, String itemCode,int quantity, String itemPrice){
    this.itemName=itemName;
    this.itemCode=itemCode;
    this.quantity=quantity;
    this.itemPrice=itemPrice;
    
    
}



public FoodItemHandler(int quantity, int numberOfItems){
    this.quantity=quantity;
    this.numberOfItems=numberOfItems;
}

public FoodItemHandler (boolean isSelected){
    this.isSelected=isSelected;
    
}

public FoodItemHandler (int numberOfItems){
    this.numberOfItems=numberOfItems;
}



public FoodItemHandler(int quantity, int numberOfItems,boolean isSelected){
    
    this.quantity=quantity;
    this.isSelected=isSelected;
     this.numberOfItems=numberOfItems;
}

public FoodItemHandler(String itemName, String itemCode,int quantity, boolean isSelected, String itemPrice){
    this.itemName=itemName;
    this.itemCode=itemCode;
    this.quantity=quantity;
    this.isSelected=isSelected;
    this.itemPrice=itemPrice;
}

public FoodItemHandler(String itemName, String itemCode,int quantity, int numberOfItems,boolean isSelected){
    this.itemName=itemName;
    this.itemCode=itemCode;
    this.quantity=quantity;
    this.isSelected=isSelected;
     this.numberOfItems=numberOfItems;
}


public void setItemCode(String newItemCode){
    itemCode=newItemCode;
  
   
}

public void setItemName(String newItemName){
    
    itemName=newItemName;
}

public void setQuantity(int newQuantity){
    
    quantity=newQuantity;
    
}

public void setIsSelected(boolean newIsSelected){
    
    isSelected=newIsSelected;
    
}

public void setItemPrice(String newItemPrice){
    
    itemPrice=newItemPrice;
}
public String getItemCode(){
    return itemCode;
}

public String getItemName(){
    return itemName;
}

public int getQuantity(){
 return quantity;   
}

public Boolean getIsSelected(){
    return isSelected;
}
    
    @Override
 public String toString(){
         
         String Code="code is: ";
        //variable used to store a statement
        String Name="name is: ";

        //variable used to store a statement
        String Price="price is:";

    //returning student name and id
    return itemCode+" "+itemName+"   "+ itemPrice;
    }    
    
    
    
}
