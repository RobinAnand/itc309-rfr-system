/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

import static ServerSide.FeedbackHandler.strDate;
import static ServerSide.Feedback_DB.feedbackUpdate;
import static ServerSide.MemberManagement_DB.getUserName;
import static ServerSide.MemberManagement_DB.recordFeedback;
import static ServerSide.MemberManagement_DB.userAlreadyExist;
import static ServerSide.Payment_DB.paymentUpdate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author 11628068
 */
public class Order_DB {
    
    static String orderTableUpdate;
    static String orderItemUpdate;
    static int orderNumber;
    static int counter=0;
    static String tablesFeedback;
    static String getTableNumber;
    
    static int getTotalItems;
    static double getTotalPrice;
    String currentDate;
    
    public void addToOrder(String custName, int numItems, String tblNo, double ttlOrderPrs){       
        
   
        String totalPrice=String.valueOf(ttlOrderPrs);
        //System.out.println("Databse"+ guestNumber);
        //System.out.println("Databse"+ uName);
        
        
//           

        
         String insertStmt =
                "INSERT INTO orders(customerName, numberOfItems, tableNumber, totalOrderPrice)VALUES('" +
                custName + "','"+ numItems+"','"+tblNo+"','"+totalPrice+ " ');";
                
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(insertStmt);
                orderTableUpdate="Record Updated";
      
        }
        catch(SQLException ex) {
            System.out.println(ex);
        // lblStatus.setText("Select failed: " + ex);
        }
    }
        
        public void addToOrderItems(int orderNumber, String itemName, String itemQty, String totalItemPrice, String tableNumber, String itemNotes){       
        
   
        //String totalPrice=String.valueOf(ttlOrderPrs);
        //System.out.println("Databse"+ guestNumber);
        //System.out.println("Databse"+ uName);
        Date date = new Date();  
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
        currentDate= formatter.format(date); 
        
//           

        
         String insertStmt =
                "INSERT INTO orderhasfooditem(orderNumber, itemName, itemQuantity, totalItemPrice, itemNotes,tableNumber,date )VALUES('" +
                orderNumber + "','"+ itemName+"','"+itemQty+"','"+totalItemPrice+"','"+itemNotes+"','"+tableNumber+"','"+currentDate+ " ');";
                
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(insertStmt);
                orderItemUpdate="Record Updated";
      
        }
        catch(SQLException ex) {
            System.out.println(ex);
        // lblStatus.setText("Select failed: " + ex);
        }
        
        
    }  
        
        
        public void viewOrderDetails(){
    
    
      try {
          
             String query = "SELECT * FROM orders  ";
   
           // Execute query
             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
             searchFromOrders(rs);
             
            }
            catch(SQLException ex) {
              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
              alert2.showAndWait(); 
              
           }
           
        }
     
     
     public void searchFromOrders(ResultSet rs) throws SQLException {
      
      while (rs.next()){
      
           orderNumber=rs.getInt(1);
           getTableNumber=rs.getString(4);
           
           
           
           System.out.println("FetchingDetails");
           counter=1;
           
      }
      
      OrderHandler.tbNumber=getTableNumber;
      
      
     } 
      
       public void addToOurTables(String tabNumber, String orderNumber, String totItemQty, String totOrderPrice){       
        
   
        //String totalPrice=String.valueOf(ttlOrderPrs);
        //System.out.println("Databse"+ guestNumber);
        //System.out.println("Databse"+ uName);
        
        String query = "UPDATE our_tables " + 
          "SET orderNumber = '" + orderNumber+ "'," +
               "totalItemQuantity='"+ totItemQty+ "'," +
               "totalOrderPrice='"+ totOrderPrice+ "'"+
                "WHERE tableNumber = '" + tabNumber + "';";
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(query);
                tablesFeedback="Record Updated";
        }
        catch(SQLException ex) {
            System.out.println(ex);
        }
        
//           

        
         
        
        
    } 
       
       
//  public void checkOrders(int orderNumber) {
//    
//    
//      try {
//          
//             String query = "SELECT * FROM orders WHERE orderNumber = "
//              + "'" + orderNumber + "'";
//   
//           // Execute query
//             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
//             searchFromOrders(rs);
//             
//            }
//            catch(SQLException ex) {
//              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
//              alert2.showAndWait(); 
//           }
//           
//        }
  
  
  
  public void updateOrders(int odNo, int itemQuantity, double tOPrice){
     
        //int orderNumber=0;
        //int numberOfItems=0;
       //double totalOrderPrice=0;
        //String status="unoccupied";
          
        String query = "UPDATE orders " + 
          "SET " +
               
               "numberOfItems='"+ itemQuantity+ "'," +
               "totalOrderPrice='"+ tOPrice+ "'"+
                "WHERE orderNumber = '" + odNo + "';";
        
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(query);
                //paymentUpdate="Payment Confirmed";
        }
        catch(SQLException ex) {
            System.out.println(ex);
        }
        
        
        
        
        
        
    }
  
  
    public void readOrders(int orderNumber){
        
        try {
          
             String query = "SELECT * FROM orders  WHERE orderNumber="+orderNumber+";";
   
           // Execute query
             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
             loadData(rs);
             
            }
            catch(SQLException ex) {
              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
              alert2.showAndWait(); 
              
           }
        
        
        
        
        
    }
    
    public void loadData(ResultSet rs) throws SQLException {
      
      while (rs.next()){
      
           getTotalItems=rs.getInt(3);
           getTotalPrice=rs.getDouble(5);
           
           
     
           //System.out.println("FetchingDetails");
       
           
      }
      
      
    }
    
    
  
  
  
    
     
     
     
     }     
      
      
      
      
      
    
     
     
     
    
         
          
        
      
          
          
          
      
      
      
      
    
      
      
      
      
            
      
    
    
    
    


