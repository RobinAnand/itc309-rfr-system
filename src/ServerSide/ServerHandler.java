/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

/**
 *
 * @author 11628068
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ServerHandler extends Application {

    TextField textTitle;
    
    Label labelSys, labelPort, labelIp;
    
    TextArea textAreaMsg;
    
    CheckBox optWelcome;

    ServerSocket serverSocket;
    
    String userNameFromClient;
    
    public static String userPasswordFromClient;
    
    static String orderName;
    
    static String orderTable;
    
    static int totalItems;
    
    static double totalOrderPrice;
    
    static int convertOrderNumber;
    
    

    @Override
    public void start(Stage primaryStage) {

        textTitle = new TextField();
        labelSys = new Label();
        labelPort = new Label();
        labelIp = new Label();
        textAreaMsg = new TextArea();
        
        //Auto scroll to bottom
        textAreaMsg.textProperty().addListener(new ChangeListener(){

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                textAreaMsg.setScrollTop(Double.MAX_VALUE); 
            }
        });
        
        optWelcome = new CheckBox("Send Welcome when connect");
        optWelcome.setSelected(true);

        labelSys.setText(
            System.getProperty("os.arch") + "/"
            + System.getProperty("os.name"));
        labelIp.setText(getIpAddress());

        VBox mainLayout = new VBox();
        mainLayout.setPadding(new Insets(5, 5, 5, 5));
        mainLayout.setSpacing(5);
        mainLayout.getChildren().addAll(textTitle,
            labelSys, labelPort, labelIp,
            optWelcome, textAreaMsg);

        StackPane root = new StackPane();
        root.getChildren().add(mainLayout);

        Scene scene = new Scene(root, 300, 400);

        primaryStage.setTitle("Android-er: JavaFX Server");
        primaryStage.setScene(scene);
        primaryStage.show();

        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.setDaemon(true); //terminate the thread when program end
       socketServerThread.start();
       
       
      
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    
    
    
    
    
    
    

    private class SocketServerThread extends Thread {

       static final int SocketServerPORT = 8088;
        int count = 0;

        @Override
        public void run() {
            try {
                Socket socket = null;
                
                serverSocket = new ServerSocket(SocketServerPORT);
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        labelPort.setText("I'm waiting here: "
                            + serverSocket.getLocalPort());
                   }
              });

                while (true) {
                    socket = serverSocket.accept();
                    count++;
                    
                    //Start another thread 
                    //to prevent blocked by empty dataInputStream
                   Thread acceptedThread = new Thread(
                        new ServerSocketAcceptedThread(socket, count));
                    acceptedThread.setDaemon(true); //terminate the thread when program end
                    acceptedThread.start();

                }
            } catch (IOException ex) {
                Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
           }
            
       }
    }
    
    //**This is for testing purpose only **//
    
    
    
    
    
    
    

    public class ServerSocketAcceptedThread extends Thread {
        
        
        
        
        
        String adminNameFromClient;
    
        String adminPasswordFromClient;
        
        String staffNameFromClient;
    
        String staffPasswordFromClient;
        
        Authentication_DB authentication_DB = new Authentication_DB();
        
        AuthenticationHandler aunthentication_Handler = new AuthenticationHandler();
        
        MenuHandler menuHandler = new MenuHandler();
        
         
        Menu_DB menuDB = new Menu_DB();
        
        ConnectionDatabase cb= new ConnectionDatabase();
        
        Order_DB orderDB = new Order_DB();
        
        OrderHandler oH =new OrderHandler();
        
        SessionHandler sessionHandler= new SessionHandler();
        
        SessionHandler_DB sessionHandlerDB= new SessionHandler_DB();
         
        MemberManagementHandler memberHandler = new MemberManagementHandler();
        
        PaymentHandler pH = new  PaymentHandler();
        
        Payment_DB pDB= new Payment_DB();
        
        KitchenHandler kitchenHandler= new KitchenHandler();
        
         
        FeedbackHandler feedbackHandler= new FeedbackHandler();
        
        AlertDB alertDB = new AlertDB();
        
        AlertHandler aH =new AlertHandler();
        
        AdminManagementHandler aMH= new AdminManagementHandler();
        
        Socket socket = null;
        
        DataInputStream dataInputStream = null;
        
        DataOutputStream dataOutputStream = null;
        int count;
        
        ArrayList<String> requestList;
        
        ArrayList<String> orderFromClient;
        
        ArrayList<String> itemCodes =new ArrayList<String>();
         
        ArrayList<String> itemNames= new ArrayList<String>();
        
        ArrayList<Integer> itemQuantity= new ArrayList<Integer>();
        
        ArrayList<Double> itemPrice= new ArrayList<Double>();
        
        ArrayList<String> itemNotes= new ArrayList<String>();
        
        
        
        
        
        
        
         
         
        
        

     public  ServerSocketAcceptedThread(){
         
         
     }
     public  ServerSocketAcceptedThread(Socket s, int c) {
            socket = s;
            count = c;
        }

        @Override
        public void run() {
            try {
                dataInputStream = new DataInputStream(
                    socket.getInputStream());
                dataOutputStream = new DataOutputStream(
                    socket.getOutputStream());

                //If dataInputStream empty, 
                //this thread will be blocked by readUTF(),
                //but not the others
                String messageFromClient = dataInputStream.readUTF();
                
                
                
                String newMessage = "#" + count + " from " + socket.getInetAddress()
                    + ":" + socket.getPort() + "\n"
                    + "Msg from client: " + messageFromClient + "\n";
                
                Platform.runLater(new Runnable() {
                    
                    @Override
                    public void run() {
                        textAreaMsg.appendText(newMessage);
                    }
                });
                
                
                
                
                
                if (optWelcome.isSelected()) {
                    
                    
                    cb.initializeDB();
                    
  
                    requestList = new ArrayList<>(Arrays.asList(messageFromClient.trim().split("//")));
                     
                    switch (requestList.get(0)){
                        
                        
                        
                        
                         case "AuthenticateUser":
                             
                             String sendToClient="";
                             
                             boolean isLegitimate;
                             
                             //authentication_DB.initializeDB();
                            
                             
                             
                            
                             userNameFromClient=requestList.get(1);
                             
                             userPasswordFromClient=requestList.get(2);
                             
                             authentication_DB.searchUser(userNameFromClient);
                             
                             isLegitimate= aunthentication_Handler.isLegitimateUser(userNameFromClient, userPasswordFromClient);
                             System.out.println(isLegitimate);
                             System.out.println(userNameFromClient);
                             
                            
                             if (isLegitimate==true){
                                 
                                 
                                 
                                 sendToClient="true";
                                
                                 System.out.println("UserFound");
                            }
                             
                             else if (isLegitimate==false){
                                 
                                 sendToClient="false";
                                 
                                 System.out.println("UserNotFound");
                             }
                             
                             
                            dataOutputStream.writeUTF(sendToClient);   
                            
                            
                            
                            
 
                            break;
                            
                            
                            
                            
                            
                            case "AuthenticateAdmin":
                             
                             String sendToClientAdminAuthority="";
                             
                             boolean isLegitimateAdmin;
                             
                            //authentication_DB.initializeDB();
                             
                             
                             
                             
                             adminNameFromClient=requestList.get(1);
                             
                             adminPasswordFromClient=requestList.get(2);
                             
                             authentication_DB.searchAdmin(adminNameFromClient);
                             
                             isLegitimateAdmin= aunthentication_Handler.IsLegitimateAdmin(adminNameFromClient, adminPasswordFromClient);
                             
                             System.out.println(isLegitimateAdmin);
                             //System.out.println(adminNameFromClient);
                             
                            
                             if (isLegitimateAdmin==true){
                                 
                                 sendToClientAdminAuthority="true";
                                
                                 System.out.println("UserFound");
                            }
                             
                            else if (isLegitimateAdmin==false){
                                 
                                 sendToClientAdminAuthority="false";
                                 
                                 System.out.println("UserNotFound");
                             }
                             
                             
                            dataOutputStream.writeUTF(sendToClientAdminAuthority);
                            
                            
                            
 
                            break;
                            
                            
                            
                            
                            case "AuthenticateStaff":
                             
                            
                            
                            String sendToClientStaffAuthority="";
                            
                            boolean isLegitimateStaff;
                             
                             //authentication_DB.initializeDB();
                             
                            
                             
                            staffNameFromClient=requestList.get(1);
                             
                            staffPasswordFromClient=requestList.get(2);
                            
                             authentication_DB.searchStaff(staffNameFromClient);
                             
                            isLegitimateStaff= aunthentication_Handler.isLegitimateStaff(staffNameFromClient, staffPasswordFromClient);
                            System.out.println(isLegitimateStaff);
                             //System.out.println(adminNameFromClient);
                             
                             
                             if (isLegitimateStaff==true){
                                 
                                sendToClientStaffAuthority="true";
                                
                                 System.out.println("UserFound");
                           }
                             
                             else if (isLegitimateStaff==false){
                                 
                                 sendToClientStaffAuthority="false";
                                 
                                 System.out.println("UserNotFound");
                             }
                             
                             
                            dataOutputStream.writeUTF(sendToClientStaffAuthority);    
 
                            break;
                            
                            
                            
                            case "GuestLogin":
                                
                             String guestName=requestList.get(1);
                             
                             //authentication_DB.searchGuestTable();
                             
                             aunthentication_Handler.GenerateGuestNumber(guestName);
                             
                             
                             
                             break;
                                
                                
                            
                            
                                //boolean isLegitimateStaff;
////                             
////                             //authentication_DB.initializeDB();
////                             
////                             
////                             
////                             
////                             staffNameFromClient=requestList.get(1);
////                             
////                             staffPasswordFromClient=requestList.get(2);
////                             
////                             authentication_DB.searchStaff(staffNameFromClient);
////                             
////                             isLegitimateStaff= aunthentication_Handler.isLegitimateStaff(staffNameFromClient, staffPasswordFromClient);
////                             System.out.println(isLegitimateStaff);
////                             //System.out.println(adminNameFromClient);
////                             
////                             
////                             if (isLegitimateStaff==true){
////                                 
////                                 sendToClientStaffAuthority="true";
////                                
////                                 System.out.println("UserFound");
////                            }
////                             
////                             else if (isLegitimateStaff==false){
////                                 
////                                 sendToClientStaffAuthority="false";
////                                 
////                                 System.out.println("UserNotFound");
////                             }
////                             
////                             
////                            dataOutputStream.writeUTF(sendToClientStaffAuthority);    
//// 
////                            break;
    
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                          
                        case"SessionManagement":   
                            
                            
                            String sendStatusToClient="";
                            String tableNumber=requestList.get(1);
                            sessionHandlerDB.searchTable(tableNumber);
                            String tableStatus=sessionHandler.tableStatus(tableNumber);
                            System.out.println("Sending STATUS"+ tableStatus);
                            
                            if(tableStatus.equals("unoccupied")){
                                
                                System.out.println("updatingClient");
                                sendStatusToClient="record updated";
                                
                            }
                            
                            else if (tableStatus.equals("occupied")){
                                System.out.println("Executing else");
                                sendStatusToClient="already occupied";
                                
                            }
                            
                            dataOutputStream.writeUTF(sendStatusToClient);  
                            
                            
                           
                        break;
                   
                        
                        
                        
                        
                      
           
                            
                        case "ViewMenus":
                            
                            menuDB.displayMenus();
                        
                            dataOutputStream.writeUTF(menuDB.send);
                            
                            
                            menuDB.send = "";
                            
                        
                        break;
                        
                        
                        
                        
                        case "StartOrder":
                            
                            orderFromClient = new ArrayList<>(Arrays.asList(messageFromClient.trim().split("//")));
                        
                            String orderName;
                            //String tableNumber;
                        
                            orderName=requestList.get(1);
                        
                            tableNumber=requestList.get(2);
                            
                        
                            //orderDB.searchFromTables(orderName, tableNumber);
                            //String tableStatus=orderDB.tableStatus;
                        
                        
                            //dataOutputStream.writeUTF(tableStatus);    
 
                        break;
                        
                        
                        
                        case "UpdateMenus":
                            
                        String itemCode=requestList.get(1);
                        String itemName=requestList.get(2);
                        String itemRate=requestList.get(3);
                        String itemDescription=requestList.get(4);
                        //System.out.println(itemCode);
                        //System.out.println(itemName);
                        //String itemRate;
                        //itemRate ="50";
                        //String itemDescription;
                        //
                        //itemDescription="This is my item";
                        
                        menuHandler.updateMenuItem(itemCode, itemName, itemRate, itemDescription);
                        
                        dataOutputStream.writeUTF(MenuHandler.sendItemUpdate);
                        
                        break;   
                        
                        
                        
                        
                        
                            
                        case "BecomeMember":
                            
                            String userName=requestList.get(1);   
                            String password=requestList.get(2);
                            String email=requestList.get(3);
                            String firstName=requestList.get(4);
                            String lastName=requestList.get(5);
                            
                        
                        
                            //String email;
                            //email="abc@gmail.com";
                            // String firstName;
                            //firstName="default";
                            //String lastName;
                            //lastName="default";
            
                        
                            memberHandler.addMember(userName, password, email, firstName, lastName);
                        
                            dataOutputStream.writeUTF(MemberManagementHandler.sendMemberUpdate);
                        
                        break;
                        
                        
                        
                        
                        case "ViewMember":
                            
                            String uName=requestList.get(1);
                            
                            memberHandler.showMemberDetails(uName);
                        
                            dataOutputStream.writeUTF(MemberManagement_DB.sendMemberDetails);
                            
                            System.err.println(MemberManagement_DB.sendMemberDetails);
                            
                            MemberManagement_DB.sendMemberDetails="";
                            
                            
                            
                            
                            
                        break;
                        
                        
                        
                        
                        case "GiveFeedback":
                            
                            String feedbackGiver=requestList.get(1);
                            String feedbackComment=requestList.get(2);
                            
                            feedbackHandler.createDate(feedbackGiver, feedbackComment);
                            
                            dataOutputStream.writeUTF(Feedback_DB.feedbackUpdate);
                            
                          
                        break;
                        
                        
                        
                        
                        
                        
                        case "ViewFeedback":
                            
                            
                            feedbackHandler.getFeedback();
                            
                            
                            if (Feedback_DB.count==0){
                                
                                dataOutputStream.writeUTF("No feedback to show");
                                
                            }
                            
                            else if (Feedback_DB.count==1){
                                
                            dataOutputStream.writeUTF(Feedback_DB.loadFeedback);
                            Feedback_DB.loadFeedback="";
                            
                            
                            }
                            
                        break;
                        
                        
                        
                        
                        case "EditMember":
                            
                            String getUserName=requestList.get(1);
                            String getUserPassword=requestList.get(2);
                            String getUserEmail=requestList.get(3);
                            String getUserFName=requestList.get(4);
                            String getUserLName=requestList.get(5);
                            
//                            String getUserEmail;
//                            getUserEmail="default@gmail.com";
//                            
//                            String getUserFName;
//                            getUserFName="default";
//                            
//                            String getUserLName;
//                            getUserLName="default";
                            
                            memberHandler.editMemberDetails(getUserName, getUserPassword, getUserEmail, getUserFName, getUserLName);
                            
                            dataOutputStream.writeUTF(MemberManagementHandler.memberUpdate);
                             
                             
                        break;
                        
                        
                        
                        case "Alert":
                            
                        String getAlertFromClient=requestList.get(1);
                        
                        aH.addAlert(getAlertFromClient);
                        dataOutputStream.writeUTF(AlertDB.alertUpdate);
                            
                            
                            
                            
                        
                        
                        break;
                                    
                        
                        case "ViewAlerts":
                            
                            
                            
                        aH.viewAlerts();
                        
                        if (AlertDB.counter==0){
                         
                            dataOutputStream.writeUTF("No alerts to show");
                            
                        }
                        
                        else if(AlertDB.counter==1){
                            
                            dataOutputStream.writeUTF(AlertDB.loadAlerts);
                            
                           
                            AlertDB.loadAlerts="";
                            
                            
                        }
                        //dataOutputStream.writeUTF(AlertDB.loadAlerts);
                        //System.out.println(AlertDB.loadAlerts);
                        
                        
                        
                        break;
                        
                        
                        
                        
                        case "Ordering":
                            
                            
                        orderFromClient = new ArrayList<>(Arrays.asList(messageFromClient.trim().split("//")));
                        
                        String getOrderNo=requestList.get(1);
                        convertOrderNumber=Integer.parseInt(getOrderNo);
                        
                        orderName=requestList.get(2);
                        
                        orderTable=requestList.get(3);
                        
                        
//                        for(int i=1; i<orderFromClient.size();i=i+5){
//                            
//                            itemCodes.add(orderFromClient.get(i));
//                            
//                          
//                           
//                        }
                        
                            
                        for (int i=4; i<orderFromClient.size();i=i+4){ 
                            
                            itemNames.add(orderFromClient.get(i));
                            System.out.println(itemNames);
                          
                                   
                        }
                        
                        for (int i=5; i<orderFromClient.size();i=i+4){ 
                            
                            itemQuantity.add(Integer.parseInt(orderFromClient.get(i)));
                            System.out.println(itemQuantity);

                                   
                        }
                        
                        for (int i=6; i<orderFromClient.size();i=i+4){ 
                            
                            itemPrice.add(Double.parseDouble(orderFromClient.get(i)));
                            System.out.println(itemPrice);

                                   
                        }
                        
                        for (int i=7; i<orderFromClient.size();i=i+4){ 
                            
                            itemNotes.add(orderFromClient.get(i));
                            System.out.println(itemNotes);

                                   
                        }
                        
                        System.out.println();
                        
                        for (int i=0; i<itemQuantity.size(); i++){
                        
                        totalItems+=itemQuantity.get(i);
                        
                        System.out.println(totalItems);
                        
                        }
                        
                        
                        for (int i=0; i<itemPrice.size(); i++){
                        
                        totalOrderPrice+=itemPrice.get(i);
                        System.out.println(totalOrderPrice);
                        
                        }
                        
                        
                        //int orderNumber=Order_DB.orderNumber;
                        
                        //orderDB.viewOrderDetails(orderTable);
                        
                        
                        
                            
                        
                      
                            oH.addToOrder(orderTable,orderName, totalItems, totalOrderPrice);
                        
                        
                            oH.getOrderNumber();
                        
                            int orderNumber=Order_DB.orderNumber;
                        
                            
                            dataOutputStream.writeUTF(String.valueOf(orderNumber));
                           
                             
                            System.out.println("Order number is this"+String.valueOf(orderNumber));
                        
                        
                        
                            for (int i=0; i<itemNames.size();i++){
                            
                            
                                String itmName=itemNames.get(i);
                                String itmQty=String.valueOf(itemQuantity.get(i));
                                String itmPrice=String.valueOf(itemPrice.get(i));
                                String itmNotes=itemNotes.get(i);
                                
                                if(Integer.parseInt(getOrderNo)==0){
                            
                                    oH.addToItems(orderNumber, itmName, itmQty,itmPrice,itmNotes, orderTable);
                                }
                                
                                else if (Integer.parseInt(getOrderNo)>0){
                                 
                                    oH.addToItems(convertOrderNumber, itmName, itmQty,itmPrice,itmNotes, orderTable);
                                    
                                }    
                                
                            }  
                            
                            
                            if(Integer.parseInt(getOrderNo)==0){
                                
                                System.out.println("Order number is this"+String.valueOf(orderNumber));
                                dataOutputStream.writeUTF(String.valueOf(orderNumber));
                                
                            }
                            
                            
                            
                        
                        
                        
                        
//                       String newOrderNumber=String.valueOf(orderNumber);
//                       String newTotalItems=String.valueOf(totalItems);
//                       String newTotalPrice=String.valueOf(totalOrderPrice);
//                         
                       
                       //oH.updateOurTables(orderTable,newOrderNumber, newTotalItems, newTotalPrice);
                       
                       
                       itemQuantity.clear();
                       itemPrice.clear();
                       itemNames.clear();
                       itemPrice.clear();
                       itemQuantity.clear();
                       itemNotes.clear();
                       totalItems=0;
                       totalOrderPrice=0;
                       
                       
                       //totalItems=0;
                       //totalOrderPrice=0;
                       
                        
                        
                        break;
                        
                        
                        
                        case "ViewBill":
                            
                        String paymentTable=requestList.get(1);
                        
                        pH.displayBill(paymentTable);
                        
                        dataOutputStream.writeUTF(Payment_DB.sendBill);
                        
                            
                            
                        break;
                        
                        
                        
                        
                        case "ClearOrder":
                            
                        
                        itemNames.clear();
                        itemPrice.clear();
                        itemQuantity.clear();
                        itemNotes.clear();
                        totalItems=0;
                        totalOrderPrice=0;
                            
                            
                        break;
                        
                        
                        
                        
                        
                        
                        case "FinalizePayment":
                            
                            
                        String tbNo=requestList.get(1);
                        System.out.println(tbNo);
                        pH.clearEntries(tbNo);
                        
                        
                        break;
                        
                        
                        
                        case "ViewKitchenOrders":
                            
                            kitchenHandler.viewKitchenScreen();
                            
                            if (Kitchen_DB.count==0){
                                
                                dataOutputStream.writeUTF("No Orders Yet");
                                
                            }
                            
                            else if (Kitchen_DB.count==1){
                                
                            dataOutputStream.writeUTF(Kitchen_DB.loadOrders);
                            
                            Kitchen_DB.loadOrders="";
                            
                            
                            
                            }
                            
                            
                            
                        break;
                            
                            
                            
                        case "UpdateAdmin":
                        
                        String adminName=requestList.get(1);
                        String adminPassword=requestList.get(2);
                        
                        aMH.changeAdminDetails(adminName, adminPassword);
                        dataOutputStream.writeUTF(AdminManagementHandler.sendAdminUpdate);
                        
                            
                        break;    
                            
                            
                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                            
                            
                    }
                            
                    String msgReply = count + ": " + textTitle.getText();
                    
                    
                }
                
            } catch (IOException ex) {
                Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (dataInputStream != null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException ex) {
                        Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (dataOutputStream != null) {
                    try {
                        dataOutputStream.close();
                    } catch (IOException ex) {
                        Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
          
            }

    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                    .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                    .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                            + inetAddress.getHostAddress() + "\n";
                    }
                }
            }
        } catch (SocketException ex) {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 

        return ip;
    }

}
