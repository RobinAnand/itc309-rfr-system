/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

/**
 *
 * @author 11628068
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 *
 * @author 11628068
 */
public class Authentication_DB {
    
    ServerHandler serverHandler = new ServerHandler();
    //Statement stmt;
    //RFOS_STORAGE rf = new RFOS_STORAGE();
    String userName;
    String password;
    
    static HashMap<String, String> userAccounts=new HashMap<>();
    static ArrayList <String> user_Name =new ArrayList<String>();
    static ArrayList <String> user_Password =new ArrayList<String>();
    
    
    static int counter;
    //AuthenticationHandler hand= new AuthenticationHandler();
    
    //static int counter=0;
    
    static String fetchGuestUserName;
    static String fetchPassword;
    static String fetchAdminUName;
    static String fetchAdminPass;
    static String fetchStaffUName;
    static String fetchStaffPass;
    static String fetchGuestNumber;
    
    
    
    
    
//   public void initializeDB() {
//       
//    try {
//        
//        System.out.println("This is accounting");
//        
//        Class.forName("com.mysql.jdbc.Driver");
//        System.out.println("Driver loaded");
//      // Connect to the local InterBase database
//      Connection conn = DriverManager.getConnection
// //   ("dbc:odbc:exampleMDBDataSource", "", "" );
//        ("jdbc:mysql://localhost/food_ordering", "root", "MySQL55!");
//      System.out.println("Database connected\n");
//
//      //lblStatus.setText("Database connected");
//
//      // Create a statement
//      stmt = conn.createStatement();
//      
//      
//    }
//    
//    catch (Exception ex) {
//        
//      Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Connection failed: "+ex,ButtonType.OK);
//      alert2.showAndWait();
//      
//    }
//    
//   } 
    
//   public void generateQuery() {
//         
//      
//    
//      try {
//          
//             String query = "SELECT * FROM memberAccounts ";
//   
//           // Execute query
//             ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
//             //searchFromAccounts(rs);
//             searchUser();
//             
//            }
//            catch(SQLException ex) {
//              Alert alert2=new Alert(Alert.AlertType.INFORMATION,"Selection failed: " +ex,ButtonType.OK);
//              alert2.showAndWait(); 
//           }
//           
//        }
//    
    
   
   public void searchUser(String userName){
       
       
       // Build a SQL SELECT statement
    String query = "SELECT * FROM memberAccounts WHERE userName = "
      + "'" + userName + "'";

     System.out.println("This is search");
     System.out.println(userName);
    try {
      // Execute query
      ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
      fetchUserData(rs);
    }
    catch(SQLException ex) {
     // lblStatus.setText("Select failed: " + ex);
    }
   }  
    
    
    public static void fetchUserData(ResultSet rs) throws SQLException {
        
        if (rs.next()){
            
            fetchGuestUserName=rs.getString(1);
            fetchPassword=rs.getString(2);
            System.out.println(fetchPassword);
        }    
        else{
            System.out.println("WrongUser");
    }    
            
  
}
    
public void searchAdmin(String userName){
       
       
       // Build a SQL SELECT statement
    String query = "SELECT * FROM adminAccounts WHERE userName = "
      + "'" + userName + "'";

     System.out.println("This is admin");
     System.out.println(userName);
    try {
      // Execute query
      ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
      fetchAdminData(rs);
    }
    catch(SQLException ex) {
     // lblStatus.setText("Select failed: " + ex);
    }
   }  
    
    
    public static void fetchAdminData(ResultSet rs) throws SQLException {
        
        if (rs.next()){
            
            fetchAdminUName=rs.getString(1);
            fetchAdminPass=rs.getString(2);
            System.out.println(fetchAdminPass);
        }    
        else{
            System.out.println("WrongUser");
    }    
            
  
    }  
    
    
    
    public void searchStaff(String userName){
       
       
       // Build a SQL SELECT statement
    String query = "SELECT * FROM staffAccounts WHERE userName = "
      + "'" + userName + "'";

     System.out.println("This is staff");
     System.out.println(userName);
    try {
      // Execute query
      ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
      fetchStaffData(rs);
    }
    catch(SQLException ex) {
     // lblStatus.setText("Select failed: " + ex);
    }
   }  
    
    
    public static void fetchStaffData(ResultSet rs) throws SQLException {
        
        if (rs.next()){
            fetchStaffUName=rs.getString(1);
            fetchStaffPass=rs.getString(2);
            System.out.println(fetchPassword);
        }    
        else{
            System.out.println("WrongUser");
    } 
    }
    


//     public void searchGuestTable(){
//       
//       
//       // Build a SQL SELECT statement
//    String query = "SELECT * FROM guestTable  ";
//
//     System.out.println("This is guest");
//     //System.out.println(userName);
//    try {
//      // Execute query
//      ResultSet rs = ConnectionDatabase.stmt.executeQuery(query);
//      fetchGuestData(rs);
//    }
//    catch(SQLException ex) {
//     // lblStatus.setText("Select failed: " + ex);
//    }
//   }  
//    
//    
//    public static void fetchGuestData(ResultSet rs) throws SQLException {
//        
//        
//        while(rs.next()){
//          
//            fetchGuestNumber=rs.getString(1);
//     
//            System.out.println("Printing fetched data"+fetchGuestNumber);
//        }
//        
//        if(!(rs.next())){
//            
//            fetchGuestNumber="0";
//            System.out.println("TableEmpty");
//        }            
//  
//    }  
//    
    
    public  void  updateGuestTable(String guestName){ 
        
        //System.out.println("Databse"+ guestNumber);
        System.out.println("Databse"+ guestName);
        
        
//            CREATE TABLE feedbacks(
//            feedbackNumber int(100) auto_increment, 
//            feedbackDate VARCHAR(100),
//            userName VARCHAR(100),
//            feedbackComment VARCHAR(800),
//            PRIMARY KEY (feedbackNumber)
//            );

        //ALTER TABLE guestTable AUTO_INCREMENT=1;
         String insertStmt =
                "INSERT INTO guestTable(guestName)VALUES('" +
                guestName + "');";
                
        
        try {
      // Execute query
                ConnectionDatabase.stmt.executeUpdate(insertStmt);
      
        }
        catch(SQLException ex) {
        // lblStatus.setText("Select failed: " + ex);
        }        
    } 
    public void setSize(int newSize){
        counter=newSize;
    
}
}

